package com.bishal.myapplication.view.fragment

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.bishal.myapplication.BR
import com.bishal.myapplication.R
import com.bishal.myapplication.common.extension.navigateDirection
import com.bishal.myapplication.data.responsemodel.ResultDetail
import com.bishal.myapplication.databinding.FragmentResultBinding
import com.bishal.myapplication.view.fragment.adapter.ResultAdapter
import com.bishal.myapplication.view.fragment.base.BaseFragment
import com.bishal.myapplication.viewmodel.MainViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

@AndroidEntryPoint
class ResultFragment : BaseFragment<FragmentResultBinding>() {
    override val layoutRes: Int = R.layout.fragment_result
    override val bindingVariable = BR.viewModel
    override val viewModel: MainViewModel by viewModels()



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupObserver()
        initialization()

    }

    private fun initialization(){
        with(dataBinding){
            nextQuiz.setOnClickListener {
                navigateDirection(ResultFragmentDirections.resultFragmentAfterQuestionFragment())
            }
        }
    }

    private fun setupObserver(){
        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.results.collectLatest {
                it?.let {
                    loadData(it)
                }
            }
        }
    }

    private fun loadData(resultsDetail:List<ResultDetail>){
        with(dataBinding){
            rvResult.apply {
                adapter = ResultAdapter(resultsDetail)
            }
        }
    }



}