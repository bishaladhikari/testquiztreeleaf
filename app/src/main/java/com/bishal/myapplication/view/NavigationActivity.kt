package com.bishal.myapplication.view

import androidx.navigation.NavController

interface NavigationActivity {
    var navController: NavController
}