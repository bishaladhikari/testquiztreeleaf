package com.bishal.myapplication.view.fragment

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import br.com.ilhasoft.support.validation.Validator
import com.bishal.myapplication.BR
import com.bishal.myapplication.R
import com.bishal.myapplication.common.extension.navigateDirection
import com.bishal.myapplication.databinding.FragmentUserBinding
import com.bishal.myapplication.persistence.sharedpref.PrefKeys
import com.bishal.myapplication.view.fragment.base.BaseFragment
import com.bishal.myapplication.viewmodel.MainViewModel
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class UserFragment : BaseFragment<FragmentUserBinding>() {
    override val layoutRes: Int = R.layout.fragment_user
    override val bindingVariable = BR.viewModel
    override val viewModel: MainViewModel by viewModels()
    private  lateinit var validator : Validator

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initialization()

    }
    private fun initialization(){
        with(dataBinding){
            validator = Validator(this)
            submitButton.setOnClickListener {
                if(validator.validate())
                sharedPrefs.put(PrefKeys.USER_NAME,tietUsername.text.toString())
                navigateDirection(UserFragmentDirections.resultFragmentAfterQuestionFragment())
            }
        }
    }



}