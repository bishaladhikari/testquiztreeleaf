package com.bishal.myapplication.view.fragment.adapter


import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bishal.myapplication.R
import com.bishal.myapplication.data.responsemodel.ResultDetail
import com.bishal.myapplication.databinding.ResultItemBinding

internal class ResultAdapter(private var itemsList: List<ResultDetail>) :
    RecyclerView.Adapter<ResultAdapter.MyViewHolder>() {
    internal inner class MyViewHolder(view: ResultItemBinding) : RecyclerView.ViewHolder(view.root) {
        val dataBinding=view
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view= ResultItemBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return MyViewHolder(view)
    }
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        with(holder.dataBinding){
            tvRound.text = tvRound.context.resources.getString(R.string.round_status,itemsList[position].id.toString())
            tvScore.text = tvScore.context.resources.getString(R.string.score_status,itemsList[position].score)
        }
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }







}
