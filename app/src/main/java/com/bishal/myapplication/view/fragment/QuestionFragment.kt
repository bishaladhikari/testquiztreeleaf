package com.bishal.myapplication.view.fragment

import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.bishal.myapplication.BR
import com.bishal.myapplication.R
import com.bishal.myapplication.common.extension.OnClickNextRound
import com.bishal.myapplication.common.extension.navigateDirection
import com.bishal.myapplication.common.extension.scoreDialog
import com.bishal.myapplication.databinding.FragmentQuestionBinding
import com.bishal.myapplication.persistence.sharedpref.PrefKeys
import com.bishal.myapplication.view.fragment.base.BaseFragment
import com.bishal.myapplication.viewmodel.MainViewModel
import com.google.android.material.card.MaterialCardView
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

@AndroidEntryPoint
class QuestionFragment : BaseFragment<FragmentQuestionBinding>(), OnClickNextRound {
    override val layoutRes: Int = R.layout.fragment_question
    override val bindingVariable = BR.viewModel
    override val viewModel: MainViewModel by viewModels()


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupObserver()
        initialization()
    }

    private fun initialization(){
        if(sharedPrefs[PrefKeys.USER_NAME, String::class.java].isEmpty()){
            navigateDirection(QuestionFragmentDirections.questionFragmentAfterUserFragment())
        }
        with(dataBinding){
            submitButton.setOnClickListener {

                if(submitButton.text == resources.getString(R.string.submit)){
                    submitButton.text = resources.getString(R.string.next)
                    viewModel?.checkQuestion()

                }else{
                    viewModel?.getNextQuestion()
                }

            }
            cvOption1.setOnClickListener {
                clearAllBackground()
                cvOption1.setBackgroundResource(R.drawable.green_gradient)
                viewModel?.selectedAnswer(1)

            }
            cvOption2.setOnClickListener {
                clearAllBackground()
                changeBackground(cvOption2,tvOption2)
                viewModel?.selectedAnswer(2)

            }
            cvOption3.setOnClickListener {
                clearAllBackground()
                changeBackground(cvOption3,tvOption3)
                viewModel?.selectedAnswer(3)

            }
            cvOption4.setOnClickListener {
                clearAllBackground()
                changeBackground(cvOption4,tvOption4)
                viewModel?.selectedAnswer(4)

            }
        }
    }
    private fun changeBackground(materialCardView: MaterialCardView,textView: TextView){
        materialCardView.setBackgroundResource(R.drawable.green_gradient)
        textView.setTextColor(ContextCompat.getColor(requireContext(), R.color.white))

    }

    private fun changeBackgroundWrong(materialCardView: MaterialCardView,textView: TextView){
        materialCardView.setBackgroundResource(R.drawable.red_gradient)
        textView.setTextColor(ContextCompat.getColor(requireContext(), R.color.white))

    }

    private fun setWrongStatus(){
        with(dataBinding){
            changeBackgroundWrong(cvOption1,tvOption1)
            changeBackgroundWrong(cvOption2,tvOption2)
            changeBackgroundWrong(cvOption3,tvOption3)
            changeBackgroundWrong(cvOption4,tvOption4)
            when(viewModel?.randomQuestions?.value?.answer){
                1->{
                    changeBackground(dataBinding.cvOption1,tvOption1)
                }
                2->{
                    changeBackground(dataBinding.cvOption2,tvOption2)
                }
                3->{
                    changeBackground(dataBinding.cvOption3,tvOption3)
                }
                4->{
                    changeBackground(dataBinding.cvOption4,tvOption4)
                }
            }
        }

    }


    private fun clearAllBackground() {
        with(dataBinding) {
            cvOption1.setBackgroundResource(R.drawable.white_gradient)
            cvOption2.setBackgroundResource(R.drawable.white_gradient)
            cvOption3.setBackgroundResource(R.drawable.white_gradient)
            cvOption4.setBackgroundResource(R.drawable.white_gradient)
            tvOption1.setTextColor(ContextCompat.getColor(requireContext(), R.color.black))
            tvOption2.setTextColor(ContextCompat.getColor(requireContext(), R.color.black))
            tvOption3.setTextColor(ContextCompat.getColor(requireContext(), R.color.black))
            tvOption4.setTextColor(ContextCompat.getColor(requireContext(), R.color.black))
            tvStatus.visibility = View.GONE
        }

    }

    
    private fun setupObserver(){
        viewModel.setTimer()
        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.questions.collectLatest {
                it?.let {
                    viewModel.getNextQuestion()
                }
            }
        }
        viewModel.randomQuestions.observe(requireActivity()){
            it?.let {
                clearAllBackground()
                viewModel.addTotalQuestion()
                dataBinding.submitButton.text = resources.getString(R.string.submit)
            }
        }

        viewModel.checkAnswer.observe(requireActivity()) {
            if(!it){
                setWrongStatus()
                setAnswerStatus(resources.getString(R.string.wrong))
            }else{
                setAnswerStatus(resources.getString(R.string.right))
                viewModel.addScore()
            }
        }
        viewModel.checkTimer.observe(requireActivity()){
            if(!it){
                viewModel.saveResult()
                loadScoreDialog()
            }
        }
        viewModel.totalQuestion.observe(requireActivity()){
            it?.let {
                if(it==10){
                    viewModel.saveResult()
                    loadScoreDialog()
                }
            }
        }
    }
    private fun loadScoreDialog(){
        requireActivity().scoreDialog(viewModel.scoreCalculation.value,this@QuestionFragment)
    }
    private fun setAnswerStatus(status:String){
        with(dataBinding){
            tvStatus.visibility = View.VISIBLE
            tvStatus.text = status
            when(status){
                resources.getString(R.string.wrong)->{
                    tvStatus.setTextColor(ContextCompat.getColor(requireContext(), R.color.color_red))
                }
                resources.getString(R.string.right)->{
                    tvStatus.setTextColor(ContextCompat.getColor(requireContext(), R.color.color_green))
                }
            }
        }
    }

    override fun onClickNextRound() {
        viewModel.totalQuestion.value = 0
        viewModel.scoreCalculation.value = 0
        viewModel.setTimer()
        viewModel.getNextQuestion()
    }

    override fun onClickCancel() {
        navigateDirection(QuestionFragmentDirections.questionFragmentAfterResultFragment())
    }

}