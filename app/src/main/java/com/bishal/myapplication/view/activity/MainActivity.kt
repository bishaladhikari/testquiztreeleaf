package com.bishal.myapplication.view.activity


import android.os.Bundle
import androidx.activity.viewModels
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import com.bishal.myapplication.R
import com.bishal.myapplication.databinding.ActivityMainBinding
import com.bishal.myapplication.BR
import com.bishal.myapplication.view.NavigationActivity
import com.bishal.myapplication.view.activity.base.BaseActivity
import com.bishal.myapplication.viewmodel.MainViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : BaseActivity<ActivityMainBinding>() , NavigationActivity {

    override val layoutRes = R.layout.activity_main
    override val bindingVariable = BR.viewModel
    override val viewModel: MainViewModel by viewModels()

    override lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initialization()

    }
    private fun initialization(){
        viewModel.fetchQuestion()
        val navHostFragment = supportFragmentManager
            .findFragmentById(R.id.nav_host_fragment_activity_main) as NavHostFragment
        navController = navHostFragment.navController
        navController.setGraph(
           R.navigation.main_navigation
        )
    }
}