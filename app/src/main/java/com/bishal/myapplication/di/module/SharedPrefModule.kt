package com.bishal.myapplication.di.module

import android.app.Application
import com.bishal.myapplication.persistence.sharedpref.SharedPrefs


import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class SharedPrefModule {

    @Provides
    @Singleton
     fun providesSharedPrefs(application: Application): SharedPrefs = SharedPrefs.getInstance(application)
}