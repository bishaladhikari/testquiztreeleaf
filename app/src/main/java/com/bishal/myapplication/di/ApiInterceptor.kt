package com.bishal.myapplication.di


import com.bishal.myapplication.persistence.sharedpref.PrefKeys
import com.bishal.myapplication.persistence.sharedpref.SharedPrefs
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject


class ApiInterceptor @Inject constructor(
    private val sharedPrefs: SharedPrefs
) : Interceptor {

    companion object {
        private const val AUTHORIZATION = "Authorization"
        private const val CHANNEL = "Channel"
        private const val DEVICEID = "DeviceId"
    }

    override fun intercept(chain: Interceptor.Chain): Response {
        val accessToken = sharedPrefs[PrefKeys.TOKEN, String::class.java]
        val deviceId = sharedPrefs[PrefKeys.DEVICE_ID, String::class.java]
        val request = chain.request().newBuilder()
            .apply {
                addHeader(CHANNEL, "Mobile")
                addHeader(
                    DEVICEID,
                    "sharedPrefs[PrefKeys.DEVICE_ID, String::class.java]")
//                if (deviceId.isNotEmpty()) addHeader(CHANNEL, "Mobile")
//                if (deviceId.isNotEmpty()) addHeader(
//                    DEVICEID,
//                    "sharedPrefs[PrefKeys.DEVICE_ID, String::class.java]")
                if (accessToken.isNotEmpty()) addHeader(AUTHORIZATION, "Bearer $accessToken")
            }
            .build()
        return chain.proceed(request)
    }
}
