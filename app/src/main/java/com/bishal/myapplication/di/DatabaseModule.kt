package com.bishal.myapplication.di

import android.content.Context
import com.bishal.myapplication.data.dao.QuizDao
import com.bishal.myapplication.data.dao.ResultDao
import com.bishal.myapplication.data.database.AppDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DatabaseModule {
    @Singleton
    @Provides
    fun providesAppDatabase(@ApplicationContext context: Context): AppDatabase =
        AppDatabase.getInstance(context)

    @Singleton
    @Provides
    fun providesQuizDao(appDatabase: AppDatabase): QuizDao =
        appDatabase.getQuizDao()


    @Singleton
    @Provides
    fun providesResultDao(appDatabase: AppDatabase): ResultDao =
        appDatabase.getResultDao()


}