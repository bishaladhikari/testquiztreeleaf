package com.bishal.myapplication.viewmodel


import android.os.CountDownTimer
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.bishal.myapplication.data.errorresponse.ApiStatus
import com.bishal.myapplication.data.errorresponse.ErrorResponse
import com.bishal.myapplication.data.responsemodel.QuestionDetail
import com.bishal.myapplication.data.responsemodel.QuestionResponse
import com.bishal.myapplication.data.responsemodel.ResultDetail
import com.bishal.myapplication.network.usecase.MainUseCase
import com.bishal.myapplication.persistence.sharedpref.SharedPrefs
import com.bishal.myapplication.viewmodel.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(private val useCase: MainUseCase, private val sharedPrefs: SharedPrefs) : BaseViewModel() {





    private val _errorMessage = MutableSharedFlow<String?>()
    val errorMessage = _errorMessage.asSharedFlow()

    private val _errorList = MutableSharedFlow<List<com.bishal.myapplication.data.errorresponse.Error>?>()
    val errorList = _errorList.asSharedFlow()

    private val _errorResponse = MutableSharedFlow<ErrorResponse?>()
    val errorResponse = _errorResponse.asSharedFlow()


    val questions = useCase.getQuestions().stateIn(viewModelScope, SharingStarted.Eagerly, null)

    val results = useCase.getResult().stateIn(viewModelScope, SharingStarted.Eagerly, null)

    val randomQuestions = MutableLiveData<QuestionDetail>(null)

    val checkTimer = MutableLiveData<Boolean>()

    val checkAnswer = MutableLiveData<Boolean>()

    val timer = MutableLiveData<String>()

    val selectOption = MutableLiveData<Int?>()

    val scoreCalculation = MutableLiveData(0)

    val totalQuestion = MutableLiveData(0)


    private val _getRandomQuestion = MutableSharedFlow<QuestionDetail?>()
    val getRandomQuestion = _getRandomQuestion.asSharedFlow()

    fun selectedAnswer(option:Int){
        selectOption.value = option
    }
    fun checkQuestion(){
        checkAnswer.value = selectOption.value == randomQuestions.value?.answer
    }
    fun addScore(){
        scoreCalculation.value = scoreCalculation.value?.plus(10)
    }
    fun addTotalQuestion(){
        totalQuestion.value = totalQuestion.value?.plus(1)
    }

    fun getNextQuestion(){
        viewModelScope.launch{
            questions.collectLatest {
                it?.let {
                    try{
                        randomQuestions.value = it.random()
                    }
                    catch (e:java.lang.Exception){

                    }

                }

            }
        }
    }

    private val _fetchQuestion = MutableSharedFlow<QuestionResponse?>()
    val fetchQuestion = _fetchQuestion.asSharedFlow()


     fun setTimer(){
        val timer = object: CountDownTimer(120000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                checkTimer.value = true
                val minutes = millisUntilFinished / 1000 / 60
                val seconds = (millisUntilFinished / 1000) % 60
                timer.value="$minutes:${getSecond(seconds)}"

            }

            override fun onFinish() {
                timer.value = "Time Up"
                checkTimer.value = false
            }
        }
        timer.start()
    }

    private fun getSecond(value:Long):String{
        return if(value<10){
            "0$value"
        }else{
            value.toString()
        }
    }





    fun fetchQuestion(){
        apiRequestInProgress.value = true

        viewModelScope.launch {
//            useCase.saveQuestions(questionDetails)
            val response = useCase.fetchQuestion()
            if (response.errorResponses?.status== ApiStatus.SUCCESS) {
                _fetchQuestion.emit(response.data)
            }
            else {
                _fetchQuestion.emit(null)
                useCase.saveQuestions(questionDetails)
                _errorResponse.emit(response.errorResponses)
                _errorMessage.emit( response.errorResponses?.status?.message)

                if (response.errorResponses?.status== ApiStatus.VALIDATION_ERROR)
                {
                    _errorList.emit( response.errorResponses?.errors)
                }


            }
            apiRequestInProgress.value = false
        }
    }




    fun saveResult(){
        apiRequestInProgress.value = true
        viewModelScope.launch {
            scoreCalculation.value?.let {
                useCase.saveResult(ResultDetail(null,it.toString()))
            }

        }
    }

    private val questionDetails = listOf(
        QuestionDetail(
            id = 1,
            question = "What is the capital of France?",
            option1 = "Paris",
            option2 = "London",
            option3 = "Berlin",
            option4 = "Madrid",
            answer = 1
        ),
        QuestionDetail(
            id = 2,
            question = "What is the largest planet in our solar system?",
            option1 = "Mercury",
            option2 = "Venus",
            option3 = "Earth",
            option4 = "Jupiter",
            answer = 4
        ),
        QuestionDetail(
            id = 3,
            question = "Who wrote the play 'Romeo and Juliet'?",
            option1 = "William Shakespeare",
            option2 = "Jane Austen",
            option3 = "Charles Dickens",
            option4 = "Mark Twain",
            answer = 1
        ),
        QuestionDetail(
            id = 4,
            question = "What is the tallest mammal on Earth?",
            option1 = "Giraffe",
            option2 = "Elephant",
            option3 = "Kangaroo",
            option4 = "Horse",
            answer = 1
        ),
        QuestionDetail(
            id = 5,
            question = "What is the capital of Japan?",
            option1 = "Tokyo",
            option2 = "Beijing",
            option3 = "Seoul",
            option4 = "Bangkok",
            answer = 1
        ),
        QuestionDetail(
            id = 6,
            question = "What is the currency of Brazil?",
            option1 = "Euro",
            option2 = "Pound",
            option3 = "Yen",
            option4 = "Real",
            answer = 4
        ),
        QuestionDetail(
            id = 7,
            question = "What is the national flower of India?",
            option1 = "Lotus",
            option2 = "Rose",
            option3 = "Tulip",
            option4 = "Sunflower",
            answer = 1
        ),
        QuestionDetail(
            id = 8,
            question = "What is the tallest mountain in the world?",
            option1 = "Mount Kilimanjaro",
            option2 = "Mount Everest",
            option3 = "Mount Fuji",
            option4 = "Mount McKinley",
            answer = 2
        ),
        QuestionDetail(
            id = 9,
            question = "Which planet is closest to the Sun?",
            option1 = "Mercury",
            option2 = "Venus",
            option3 = "Earth",
            option4 = "Mars",
            answer = 1
        ),
        QuestionDetail(
            id = 10,
            question = "What is the largest organ in the human body?",
            option1 = "Heart",
            option2 = "Brain",
            option3 = "Skin",
            option4 = "Lungs",
            answer = 3
        ),
        QuestionDetail(
            id = 11,
            question = "What is the largest ocean on Earth?",
            option1 = "Indian Ocean",
            option2 = "Atlantic Ocean",
            option3 = "Arctic Ocean",
            option4 = "Pacific Ocean",
            answer = 4
        ),
        QuestionDetail(
            id = 12,
            question = "What is the world's highest mountain?",
            option1 = "Mount Everest",
            option2 = "Kilimanjaro",
            option3 = "Matterhorn",
            option4 = "Mount McKinley",
            answer = 1
        ),
        QuestionDetail(
            id = 13,
            question = "What is the largest planet in our solar system?",
            option1 = "Mars",
            option2 = "Jupiter",
            option3 = "Saturn",
            option4 = "Neptune",
            answer = 2
        ),
        QuestionDetail(
            id = 14,
            question = "Who wrote the play 'Romeo and Juliet'?",
            option1 = "William Shakespeare",
            option2 = "Charles Dickens",
            option3 = "Jane Austen",
            option4 = "Mark Twain",
            answer = 1
        ),
        QuestionDetail(
            id = 15,
            question = "What is the capital of Australia?",
            option1 = "Melbourne",
            option2 = "Sydney",
            option3 = "Canberra",
            option4 = "Perth",
            answer = 3
        ),
        QuestionDetail(
            id = 16,
            question = "Which gas makes up the majority of Earth's atmosphere?",
            option1 = "Oxygen",
            option2 = "Carbon dioxide",
            option3 = "Nitrogen",
            option4 = "Argon",
            answer = 3
        ),
        QuestionDetail(
            id = 17,
            question = "What is the largest species of shark?",
            option1 = "Tiger shark",
            option2 = "Great white shark",
            option3 = "Whale shark",
            option4 = "Hammerhead shark",
            answer = 3
        ),
        QuestionDetail(
            id = 18,
            question = "What is the national bird of the United States?",
            option1 = "Eagle",
            option2 = "Ostrich",
            option3 = "Penguin",
            option4 = "Peacock",
            answer = 1
        ),
        QuestionDetail(
            id = 19,
            question = "What is the largest continent on Earth?",
            option1 = "North America",
            option2 = "Africa",
            option3 = "Asia",
            option4 = "Europe",
            answer = 3
        ),
        QuestionDetail(
            id = 20,
            question = "What is the fastest land animal?",
            option1 = "Lion",
            option2 = "Cheetah",
            option3 = "Gazelle",
            option4 = "Leopard",
            answer = 2
        )
    )



}