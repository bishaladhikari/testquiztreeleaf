package com.bishal.myapplication.network

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.util.Log
import android.widget.Toast
import com.bishal.myapplication.BuildConfig
import com.bishal.myapplication.common.extension.gettingResource
import com.bishal.myapplication.common.logger.Logger
import com.bishal.myapplication.data.errorresponse.ApiStatus
import com.bishal.myapplication.data.generic.GenericResponse
import com.google.gson.Gson
import com.google.gson.stream.MalformedJsonException
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Response
import com.bishal.myapplication.data.errorresponse.Error
import java.net.ConnectException
import javax.inject.Singleton
import com.bishal.myapplication.data.errorresponse.ErrorResponse
import com.bishal.myapplication.data.errorresponse.ListErrorResponse

private const val TAG = "IRepository"
@Suppress("UNCHECKED_CAST")
@Singleton
interface BaseRepository {

    //list in only 422 error
    fun <T> handleApiFailureResponse(apiResponse: Response<GenericResponse<T>>, context: Context?): GenericResponse.ApiDefaultResponse<T> {
        var errorCode: ApiStatus = ApiStatus.UNKNOWN
        val errorBody=apiResponse.errorBody()

        if(errorBody==null)
        {
 Log.d("errorbody null","came here")

            when(apiResponse.code()){
                404->{
                    errorCode= ApiStatus.NOT_FOUND
                    errorCode = context.gettingResource(errorCode)
                }
                else ->{
                    errorCode = ApiStatus.UNKNOWN
                    if(BuildConfig.DEBUG) errorCode.message = "Http Exception with error code ${apiResponse.code()}"
                    else errorCode = context.gettingResource(errorCode)
                }

            }
        }

        errorBody?.let {

                val jsonObject = JSONObject(it.string())
            it.string()
                try {
                    val gson = Gson()
                    val response = jsonObject.getString("response")
                    val status = gson.fromJson(
                        response,
                        com.bishal.myapplication.data.generic.Response::class.java
                    )

                     var errors:  List<Error> = emptyList()
                    if(status.status==422){
                       val dataString = jsonObject.getString("data").toString()

                        val errorResponse = gson.fromJson(dataString, ListErrorResponse::class.java)
                        errors = errorResponse.errors

                    }



//                 log out for the certain status code which indicates token expire
                    errorCode = when (status.status) {

                        401 -> ApiStatus.SESSION_EXPIRED
                        404 -> ApiStatus.NOT_FOUND
                        400 -> ApiStatus.INVALID_REQUEST
                        422 -> ApiStatus.VALIDATION_ERROR
                        500->  ApiStatus.INTERNAL_SERVER_ERROR
                        else -> ApiStatus.UNKNOWN
                    }
                    Log.e("Error_M",status.message)

                    if(errorCode == ApiStatus.INVALID_REQUEST){

                        context.gettingResource(errorCode).message=status.message
                    }
                    return GenericResponse.ApiDefaultResponse(
                        null, status,
                        ErrorResponse(context.gettingResource(errorCode),errors)
                    )

                }

                catch (ex: Exception) {

                    Log.d("rantestexception",ex.toString())
                    errorCode = if(ex is JSONException){
                        Logger.Debug(
                            "Exception","Json parse exception"
                        )
                        context.gettingResource(ApiStatus.BAD_RESPONSE)
                    }else{
                        context.gettingResource(ApiStatus.EXCEPTION_OCCURED)
                    }


                    ex.message?.let { value ->

                        if(BuildConfig.DEBUG)
                            errorCode.message = value
                    }

                    Logger.Debug(
                        "BaseRepository",
                        ex.message ?: "Unknown error while handling failure response"
                    )
                }



        }

        return GenericResponse.ApiDefaultResponse(null, null, ErrorResponse(errorCode))
    }


    private fun <T> handleApiException(context: Context?,exception: Exception?): GenericResponse.ApiDefaultResponse<T> {
        Log.e("ExceptionTest","kjgkjbk")
        var apiStatus: ApiStatus = ApiStatus.UNKNOWN
        exception?.let {
            apiStatus = when (exception) {
                is ConnectException -> {
                    ApiStatus.NO_NETWORK
                }
                is MalformedJsonException -> {
                    ApiStatus.NOT_FOUND
                }
                is JSONException ->{
                    ApiStatus.NOT_FOUND
                }

                else -> {

                    ApiStatus.EXCEPTION_OCCURED
                }
            }
            if (BuildConfig.DEBUG){
                apiStatus.message = exception.message.toString()
                Log.e("Exception_error",apiStatus.message)
            }

            return GenericResponse.ApiDefaultResponse(
                null, null,
                ErrorResponse(context.gettingResource(apiStatus))
            )
        }
        return GenericResponse.ApiDefaultResponse(null, null, ErrorResponse(context.gettingResource(apiStatus)))
    }

    private fun checkInternet(context: Context?): Boolean {

        // register activity with the connectivity manager service
        val connectivityManager =
            context?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        // if the android version is equal to M
        // or greater we need to use the
        // NetworkCapabilities to check what type of
        // network has the internet connection

        // Returns a Network object corresponding to
        // the currently active default data network.
        val network = connectivityManager.activeNetwork ?: return false

        // Representation of the capabilities of an active network.
        val activeNetwork = connectivityManager.getNetworkCapabilities(network) ?: return false

        return when {
            // Indicates this network uses a Wi-Fi transport,
            // or WiFi has network connectivity
            activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true

            // Indicates this network uses a Cellular transport. or
            // Cellular has network connectivity
            activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true

            // else return false
            else -> false
        }

    }


    suspend fun <T> handleApiRequest(
        call: suspend () -> Response<GenericResponse<T>>,
        context: Context?
    ): GenericResponse.ApiDefaultResponse<T> {
        return try {
            if (checkInternet(context)) {
                val apiResponse = call.invoke()
                if (apiResponse.isSuccessful) {
                    apiResponse.errorBody()?.string()?.let {
                        JSONObject(it)
                    }
//                    Log.e("TestMess",apiResponse.toString())
                    if (apiResponse.body()?.response?.status == 200)
                     GenericResponse.ApiDefaultResponse(
                            apiResponse.body()?.data,
                            apiResponse.body()?.response,
                            ErrorResponse(ApiStatus.SUCCESS)

                        )
                    else {
                        GenericResponse.ApiDefaultResponse(
                            apiResponse.body()?.data,
                            apiResponse.body()?.response,
                            ErrorResponse(ApiStatus.UNKNOWN)
                        )

                    }
                } else {
                    if(apiResponse.code()==401){
                        Toast.makeText(context,"Session Expired",Toast.LENGTH_SHORT).show()

                    }
//                    val obj = apiResponse.re()?.string()?.let { JSONObject(it) }
//                    Log.e("TestMess",apiResponse.toString())
                    handleApiFailureResponse(apiResponse,context)
                }


            } else {
                GenericResponse.ApiDefaultResponse(
                    null,
                    null,
                    ErrorResponse(ApiStatus.NO_INTERNET)
                )
            }
        } catch (ex: Exception) {

            Log.d("exception occured",ex.toString())
            handleApiException(context,ex)
        }

    }


}



