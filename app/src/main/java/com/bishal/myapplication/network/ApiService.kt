package com.bishal.myapplication.network

import com.bishal.myapplication.data.generic.GenericResponse
import com.bishal.myapplication.data.responsemodel.QuestionResponse
import com.bishal.myapplication.persistence.constant.Constants
import retrofit2.Response
import retrofit2.http.*


interface ApiService {
    @GET(Constants.GET_QUESTION)
    suspend fun getCountryList() : Response<GenericResponse<QuestionResponse>>

}