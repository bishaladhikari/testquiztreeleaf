package com.bishal.myapplication.network.repository.base
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.util.Log
import com.bishal.myapplication.common.logger.Logger
import com.bishal.myapplication.network.remote.response.ApiResponse
import com.bishal.myapplication.network.remote.response.ErrorCode
import com.bishal.myapplication.network.remote.response.ErrorResponse
import com.google.gson.stream.MalformedJsonException
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Response
import java.net.ConnectException

private const val TAG = "IRepository"

interface  BaseRepository  {


    suspend fun <T> handleRequest(call: suspend () -> Response<T>,context: Context?): ApiResponse<T> {
        return try {

            if(checkInternet(context))
            {
                val apiResponse = call.invoke()
                if (apiResponse.isSuccessful) {
                    ApiResponse.Success(apiResponse.body())
                } else {
                    handleFailureResponse(apiResponse.errorBody())
                }
            }
            else{

                Log.d ("irepository", "There is no internet")
                 ApiResponse.Failure(ErrorResponse(ErrorCode.NO_INTERNET))

            }
        } catch (ex: Exception) {
            Logger.Error(TAG, ex.localizedMessage)
            ex.printStackTrace()
            handleException(ex)
        }
    }

    fun <T> handleFailureResponse(response: ResponseBody?): ApiResponse<T> {
        response?.let {
            val jsonObject = JSONObject(it.string())
            try {

                //working on redirect to login after session expired
                val code = jsonObject.getInt("status")
                val error = jsonObject.getString("error")
                val errorMessage = jsonObject.getString("message")

                val errorCode: ErrorCode = when (code) {
                    401 -> ErrorCode.UNAUTHORIZED
                    404 -> ErrorCode.NOT_FOUND
                    else -> ErrorCode.UNKNOWN
                }

                return ApiResponse.Failure(ErrorResponse(errorCode, error, errorMessage))
            } catch (ex: Exception) {
                Logger.Debug(
                    "BaseRepository",
                    ex.message ?: "Unknown error while handling failure response"
                )
            }
        }
        return ApiResponse.Failure(ErrorResponse(ErrorCode.UNKNOWN))
    }

    private fun <T> handleException(exception: Exception?): ApiResponse<T> {
        exception?.let {
            val errorResponse = when (exception) {
                is ConnectException -> {
                    ErrorResponse(ErrorCode.NO_NETWORK)
                }
                is MalformedJsonException -> {
                    ErrorResponse(ErrorCode.BAD_RESPONSE)
                }
                else -> {
                    ErrorResponse(ErrorCode.UNKNOWN)
                }
            }
            return ApiResponse.Failure(errorResponse)
        }
        return ApiResponse.Failure(ErrorResponse(ErrorCode.UNKNOWN))
    }


   private fun  checkInternet(context: Context?) : Boolean {



       // register activity with the connectivity manager service
       val connectivityManager = context?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

       // if the android version is equal to M
       // or greater we need to use the
       // NetworkCapabilities to check what type of
       // network has the internet connection

       // Returns a Network object corresponding to
       // the currently active default data network.
       val network = connectivityManager.activeNetwork ?: return false

       // Representation of the capabilities of an active network.
       val activeNetwork = connectivityManager.getNetworkCapabilities(network) ?: return false

       return when {
           // Indicates this network uses a Wi-Fi transport,
           // or WiFi has network connectivity
           activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true

           // Indicates this network uses a Cellular transport. or
           // Cellular has network connectivity
           activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true

           // else return false
           else -> false
       }

   }




        }



