package com.bishal.myapplication.network.usecase

import android.content.Context
import com.bishal.myapplication.data.dao.QuizDao
import com.bishal.myapplication.data.dao.ResultDao
import com.bishal.myapplication.data.generic.GenericResponse
import com.bishal.myapplication.data.responsemodel.QuestionDetail
import com.bishal.myapplication.data.responsemodel.QuestionResponse
import com.bishal.myapplication.data.responsemodel.ResultDetail

import com.bishal.myapplication.network.BaseRepository
import com.bishal.myapplication.network.ApiService
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.distinctUntilChanged
import javax.inject.Inject
import javax.inject.Singleton


@Suppress("UNCHECKED_CAST")
@Singleton
class MainUseCase @Inject constructor(private  val service: ApiService, @ApplicationContext val context: Context,private val quizDao: QuizDao
                                      ,private val resultDao: ResultDao):
    BaseRepository {

    suspend fun fetchQuestion(): GenericResponse.ApiDefaultResponse<QuestionResponse> {
        return handleApiRequest({ service.getCountryList() }, context)
    }


     fun getQuestions(): Flow<List<QuestionDetail>> {
        return quizDao.getQuestions().distinctUntilChanged()
    }

    fun getResult(): Flow<List<ResultDetail>> {
        return resultDao.getResults().distinctUntilChanged()
    }

    suspend fun saveResult(result:ResultDetail) {
        resultDao.insert(result)
    }


    suspend fun saveQuestions(questions:List<QuestionDetail>) {
        quizDao.deleteAll()
         quizDao.insert(*questions.toTypedArray())
    }


    suspend fun deleteQuestions() {
        quizDao.deleteAll()
    }

    suspend fun deleteResult() {
        resultDao.deleteAll()
    }

}