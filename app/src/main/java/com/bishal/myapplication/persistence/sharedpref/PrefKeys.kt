package com.bishal.myapplication.persistence.sharedpref

/**
 * Define constants here
 * e.g.
 * const val EXTRA_PARAM_NAME = "extra_param_name"
 */
final object PrefKeys {
    val USER_NAME="USER_NAME"
    val DEVICE_ID="DEVICE_ID"
    val TOKEN="TOKEN"

}