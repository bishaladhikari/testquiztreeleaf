package com.bishal.myapplication.data.responsemodel

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import java.io.Serializable
@Entity(
    tableName = "quiz_table",
    indices = [Index(value = ["id"], unique = true)]
)
data class QuestionDetail(
    @PrimaryKey
    val id: Int,
    val question: String,
    val option1: String,
    val option2: String,
    val option3: String,
    val option4: String,
    val answer: Int
): Serializable