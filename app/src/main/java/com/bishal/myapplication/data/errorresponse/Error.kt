package com.bishal.myapplication.data.errorresponse

data class Error(
    val `field`: String,
    val message: String
)