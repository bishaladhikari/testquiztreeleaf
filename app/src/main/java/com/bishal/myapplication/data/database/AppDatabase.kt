package com.bishal.myapplication.data.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.bishal.myapplication.data.dao.QuizDao
import com.bishal.myapplication.data.dao.ResultDao
import com.bishal.myapplication.data.responsemodel.QuestionDetail
import com.bishal.myapplication.data.responsemodel.ResultDetail


@Database(
    entities = [QuestionDetail::class,ResultDetail::class],
    version = 1,
    exportSchema = false
)

abstract class AppDatabase : RoomDatabase() {
    companion object {
        private const val databaseName = "quiz-database"

        @Volatile
        private var instance: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase = instance ?: synchronized(this) {
            Room.databaseBuilder(context, AppDatabase::class.java, databaseName).build()
        }.also {
            instance = it
        }
    }

    abstract fun getQuizDao(): QuizDao
    abstract fun getResultDao(): ResultDao

}