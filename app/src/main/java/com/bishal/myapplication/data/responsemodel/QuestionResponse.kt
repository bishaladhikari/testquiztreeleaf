package com.bishal.myapplication.data.responsemodel

import java.io.Serializable

data class QuestionResponse(
    val questions:List<QuestionDetail>
): Serializable