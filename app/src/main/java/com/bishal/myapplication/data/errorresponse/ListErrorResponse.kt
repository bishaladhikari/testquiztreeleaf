package com.bishal.myapplication.data.errorresponse



data class ListErrorResponse(
    val errors: List<Error> = emptyList()
)