package com.bishal.myapplication.data.responsemodel

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import java.io.Serializable
@Entity(
    tableName = "result_table",
    indices = [Index(value = ["id"], unique = true)]
)
data class ResultDetail(
    @PrimaryKey(autoGenerate = true)
    val id: Int?,
    val score: String
): Serializable