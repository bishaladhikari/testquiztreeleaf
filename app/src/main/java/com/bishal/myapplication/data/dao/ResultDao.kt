package com.bishal.myapplication.data.dao

import androidx.room.Dao
import androidx.room.Query
import com.bishal.myapplication.data.dao.base.BaseDao
import com.bishal.myapplication.data.responsemodel.ResultDetail
import kotlinx.coroutines.flow.Flow

@Dao
interface ResultDao : BaseDao<ResultDetail> {
    @Query("SELECT * FROM result_table")
    fun getResults(): Flow<List<ResultDetail>>



    @Query("DELETE FROM result_table")
    suspend fun deleteAll()
}