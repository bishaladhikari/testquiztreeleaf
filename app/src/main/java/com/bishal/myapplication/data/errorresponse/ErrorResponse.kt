package com.bishal.myapplication.data.errorresponse

enum class ApiStatus (var message: String= "")  {

    EXCEPTION_OCCURED,
    INVALID_REQUEST ,
    UNAUTHORIZED ,
    INTERNAL_SERVER_ERROR ,
    NOT_FOUND,
    NO_NETWORK,
    BAD_RESPONSE,
    UNKNOWN,
    NO_INTERNET,
    SESSION_EXPIRED,
    VALIDATION_ERROR,
    SUCCESS


}


data class ErrorResponse(val status: ApiStatus = ApiStatus.UNKNOWN,
                         val errors:List<Error> = emptyList()) {
    override fun toString(): String {
        return status.name
    }
}