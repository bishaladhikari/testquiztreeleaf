package com.bishal.myapplication.data.generic

data class Response(
    val message: String,
    val status: Int
)