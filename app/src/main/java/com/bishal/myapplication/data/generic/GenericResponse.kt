package com.bishal.myapplication.data.generic

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.bishal.myapplication.data.errorresponse.ErrorResponse


open class GenericResponse<T>(
    @SerializedName("data")
    @Expose
    val data : T?,

    @SerializedName("response")
    @Expose
    val response: Response?,

    val errordata: ErrorResponse?
) {

    data class ApiDefaultResponse<T>(val datas : T? =null, val responses: Response?=null, var errorResponses: ErrorResponse?) : GenericResponse<T>(datas,responses,errorResponses)
}