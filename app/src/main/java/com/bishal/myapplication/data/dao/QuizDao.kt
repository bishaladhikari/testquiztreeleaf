package com.bishal.myapplication.data.dao

import androidx.room.Dao
import androidx.room.Query
import com.bishal.myapplication.data.dao.base.BaseDao
import com.bishal.myapplication.data.responsemodel.QuestionDetail
import kotlinx.coroutines.flow.Flow

@Dao
interface QuizDao : BaseDao<QuestionDetail> {
    @Query("SELECT * FROM quiz_table")
    fun getQuestions(): Flow<List<QuestionDetail>>



    @Query("DELETE FROM quiz_table")
    suspend fun deleteAll()
}