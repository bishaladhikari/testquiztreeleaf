package com.bishal.myapplication.common.extension

import android.app.Activity
import android.content.Context
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.View
import android.widget.LinearLayout
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import com.bishal.myapplication.R
import com.bishal.myapplication.data.errorresponse.ApiStatus
import com.bishal.myapplication.databinding.ScoreDialogBinding
import java.util.*


fun Context?.gettingResource(apiStatus: ApiStatus): ApiStatus {
    val error: ApiStatus =apiStatus

    error.message=when(apiStatus){
        ApiStatus.VALIDATION_ERROR->{
            "this!!.getString(R.string.validation_error)"
        }
        ApiStatus.UNKNOWN->{
            "this!!.getString(R.string.something_went_wrong)"
        }
        ApiStatus.SESSION_EXPIRED->{
            "this!!.getString(R.string.session_expired)"
        }
        ApiStatus.INVALID_REQUEST->{
            apiStatus.message
        }
        ApiStatus.NOT_FOUND->{
            "this!!.getString(R.string.error_not_found)"
        }
        ApiStatus.BAD_RESPONSE->{
            "this!!.getString(R.string.api_response_is_a_bad_json)"
        }
        ApiStatus.NO_INTERNET->{
            "this!!.getString(R.string.no_internet)"
        }
        ApiStatus.SUCCESS->{
            "this!!.getString(R.string.success)"
        }
        ApiStatus.UNAUTHORIZED->{
            "this!!.getString(R.string.error_not_found)"
        }
        ApiStatus.EXCEPTION_OCCURED->{
           " this!!.getString(R.string.exception_occured)"
        }

        else->{
           " this!!.getString(R.string.something_went_wrong)"
        }
    }


    return error
}

private var progressDialog: AlertDialog? = null
private var dialogDisplayedTimeInMillis: Long = 0
private const val preferredDelayInMillis: Long = 750
fun Activity.showProgressDialog() {
    progressDialog?.dismiss()
    val view = layoutInflater.inflate(R.layout.progress_dialog, null)
    progressDialog = AlertDialog.Builder(this)
        .setView(view)
        .setCancelable(false)
        .create()
    progressDialog?.show()
        .also { dialogDisplayedTimeInMillis = Calendar.getInstance().timeInMillis }
    progressDialog?.window?.apply {
        setLayout(
            LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        setBackgroundDrawableResource(android.R.color.transparent)
    }
}
fun Fragment.showProgressDialog() {
    requireActivity().showProgressDialog()
}
fun hideProgressDialog() {
    val timeElapsed = Calendar.getInstance().timeInMillis - dialogDisplayedTimeInMillis
    if (timeElapsed > preferredDelayInMillis) {
        progressDialog?.dismiss()
    } else {
        if(progressDialog !=null)
            Handler(Looper.getMainLooper()).postDelayed({
                if(progressDialog?.isShowing == true)
                    try {
                        progressDialog?.dismiss()
                    } catch (ex: Exception) {
                        Log.e("TAG", ex.message, ex)
                    }


            }, preferredDelayInMillis - timeElapsed)
    }


}


var customDialog: AlertDialog? = null
fun Activity.scoreDialog(
    score:Int?,
    onClickNextRound: OnClickNextRound
) {
    customDialog?.dismiss()
    val dialogView = ScoreDialogBinding.inflate(layoutInflater)
    customDialog = AlertDialog.Builder(this)
        .setCancelable(false)
        .setView(dialogView.root)
        .create()
    dialogView.apply {

        nextButton.setOnClickListener {
            onClickNextRound.onClickNextRound()
            customDialog?.dismiss()
        }
        ibBack.setOnClickListener {
            onClickNextRound.onClickCancel()
            customDialog?.dismiss()
        }
        score?.let {
            pb.progress = score
            progressTv.text = score.toString()
        }


    }
    customDialog?.show()
    customDialog?.window?.apply {
        setLayout(
            LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        setBackgroundDrawableResource(android.R.color.transparent)
    }
}

fun Fragment.navigateDirection(navDirections: NavDirections) {
    return findNavController().navigate(navDirections)
}
interface OnClickNextRound{
    fun onClickNextRound()
    fun onClickCancel()
}