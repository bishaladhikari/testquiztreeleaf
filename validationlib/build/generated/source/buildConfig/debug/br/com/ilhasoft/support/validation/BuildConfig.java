/**
 * Automatically generated file. DO NOT MODIFY
 */
package br.com.ilhasoft.support.validation;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String LIBRARY_PACKAGE_NAME = "br.com.ilhasoft.support.validation";
  public static final String BUILD_TYPE = "debug";
}
